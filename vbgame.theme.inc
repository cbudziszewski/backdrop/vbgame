<?php
/**
 * @file
 * Theming definitions for vbgame content types
 */

/**
 * Theme next TOP match.
 */
function theme_vbgame_topmatch_preview($next_games) {
  $content = '';

  foreach ($next_games as $next_game) {
    $content .= '<p>';
    $content .= t('on @datum at @zeit', @array('@datum' => $next_game->datum, '@zeit' => $next_game->beginn));
    $content .= '<br /><strong>';
    $content .= $next_game->name . ' vs. ';
    $content .= check_plain($next_game->heimspiel ? $next_game->gast : $next_game->heim);
    $content .= '</strong><br />';
    $content .= $next_game->halle;
    $content .= '<br /><strong>';
    $content .= $next_game->heimspiel ? t('HOME MATCH') : t('AWAY MATCH');
    $content .= '</strong>';
    $content .= '</p>';
  }
  return $content;
}

function theme_vbgame_ranking_threepoint_all($variables) {
  return theme_vbgame_ranking_threepoint_all_vertical($variables);
}
/**
 * Theme ranking data.
 *
 * Full threepoint information
 */
function theme_vbgame_ranking_threepoint_all_horizontal($variables) {
  $content = $variables['content'];
  $teaminfo = $variables['teaminfo'];

  $attributes = array(
    'class' => 'vbgame_tabelle',
  );
  $header = array(
    t('place'),
    t('team'),
    t('matches played'),
    t('wins'),
    t('losses'),
    t('points'),
    t('set points'),
    t('set win score'),
    t('set lose score'),
    t('set point difference'),
    t('set quotient'),
    t('ball points'),
    t('ball win score'),
    t('ball lose score'),
    t('ball point difference'),
    t('ball quotient'),
    t('3:0'),
    t('3:1'),
    t('3:2'),
    t('2:3'),
    t('1:3'),
    t('0:3'),
  );

  for ($i = 0; $i != count($content); ++$i) {
    $special = strpos($content[$i]->team, $teaminfo->pattern) !== FALSE;
    $rows[] = array(
      'data' => array(
        check_plain($content[$i]->place),
        check_plain($content[$i]->team),
        check_plain($content[$i]->matchesPlayed),
        check_plain($content[$i]->wins),
        check_plain($content[$i]->losses),
        check_plain($content[$i]->points),
        check_plain($content[$i]->setPoints),
        check_plain($content[$i]->setWinScore),
        check_plain($content[$i]->setLoseScore),
        check_plain($content[$i]->setPointDifference),
        check_plain($content[$i]->setQuotient),
        check_plain($content[$i]->ballPoints),
        check_plain($content[$i]->ballWinScore),
        check_plain($content[$i]->ballLoseScore),
        check_plain($content[$i]->ballPointDifference),
        check_plain($content[$i]->ballQuotient),
        check_plain($content[$i]->count30),
        check_plain($content[$i]->count31),
        check_plain($content[$i]->count32),
        check_plain($content[$i]->count23),
        check_plain($content[$i]->count13),
        check_plain($content[$i]->count03),
      ),
      'class' => _vbgame_tr_attributes($i, $special),
    );
  }

  $html = "\n<!-- theme_vbgame_ranking_threepoint_all --> \n";
  $html .= '<div class="vbgame_data">';
  $html .= theme('table', $header, $rows, $attributes);
  $html .= '</div>';
  $html .= theme('vbgame_srcinfo', array($teaminfo));

  return $html;
}

/**
 * Theme ranking data.
 *
 * Full threepoint information
 */
function theme_vbgame_ranking_threepoint_all_vertical($variables) {
  $content = $variables['content'];
  $teaminfo = $variables['teaminfo'];

  $attributes = array(
    'class' => 'vbgame_tabelle_vertical',
  );
  $header[1] = array(
    array(
      'data' => t('place'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('team'),
      'class' => '',
    ),
    array(
      'data' => t('matches played'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('wins'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('losses'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('points'),
      'class' => 'vbgame_table_cell_tight',
    ),
  );
  $header[2] = array(
    array(
      'data' => t('place'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('team'),
      'class' => '',
    ),
    array(
      'data' => t('set points'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('set win score'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('set lose score'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('set point difference'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('set quotient'),
      'class' => 'vbgame_table_cell_tight',
    ),
  );
  $header[3] = array(
    array(
      'data' => t('place'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('team'),
      'class' => '',
    ),
    array(
      'data' => t('ball points'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('ball win score'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('ball lose score'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('ball point difference'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('ball quotient'),
      'class' => 'vbgame_table_cell_tight',
    ),
  );
  $header[4] = array(
    array(
      'data' => t('place'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('team'),
      'class' => '',
    ),
    array(
      'data' => t('3:0'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('3:1'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('3:2'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('2:3'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('1:3'),
      'class' => 'vbgame_table_cell_tight',
    ),
    array(
      'data' => t('0:3'),
      'class' => 'vbgame_table_cell_tight',
    ),
  );

  for ($i = 0; $i != count($content); ++$i) {
    $special = strpos($content[$i]->team, $teaminfo->pattern) !== FALSE;
    $rows[1][] = array(
      'data' => array(
        check_plain($content[$i]->place),
        check_plain($content[$i]->team),
        check_plain($content[$i]->matchesPlayed),
        check_plain($content[$i]->wins),
        check_plain($content[$i]->losses),
        check_plain($content[$i]->points),
      ),
      'class' => _vbgame_tr_attributes($i, $special),
    );
    $rows[2][] = array(
      'data' => array(
        check_plain($content[$i]->place),
        check_plain($content[$i]->team),
        check_plain($content[$i]->setPoints),
        check_plain($content[$i]->setWinScore),
        check_plain($content[$i]->setLoseScore),
        check_plain($content[$i]->setPointDifference),
        check_plain($content[$i]->setQuotient),
      ),
      'class' => _vbgame_tr_attributes($i, $special),
    );
    $rows[3][] = array(
      'data' => array(
        check_plain($content[$i]->place),
        check_plain($content[$i]->team),
        check_plain($content[$i]->ballPoints),
        check_plain($content[$i]->ballWinScore),
        check_plain($content[$i]->ballLoseScore),
        check_plain($content[$i]->ballPointDifference),
        check_plain($content[$i]->ballQuotient),
      ),
      'class' => _vbgame_tr_attributes($i, $special),
    );
    $rows[4][] = array(
      'data' => array(
        check_plain($content[$i]->place),
        check_plain($content[$i]->team),
        check_plain($content[$i]->count30),
        check_plain($content[$i]->count31),
        check_plain($content[$i]->count32),
        check_plain($content[$i]->count23),
        check_plain($content[$i]->count13),
        check_plain($content[$i]->count03),
      ),
      'class' => _vbgame_tr_attributes($i, $special),
    );
  }

  $html = "\n<!-- theme_vbgame_ranking_threepoint_all --> \n";
  $html .= '<div class="vbgame_data">';
  for ($i = 1; $i <= 4; $i++) {
    $html .= theme('table', $header[$i], $rows[$i], $attributes);
  }
  $html .= '</div>';
  $html .= theme('vbgame_srcinfo', array($teaminfo));

  return $html;
}

/**
 * Theme Team Content.
 */
function theme_vbgame_team_content($variables) {
  $type = $variables['type'];
  $content = $variables['content'];
  $info = $variables['teaminfo'];

  switch ($type) {
    case '3P_ranking_deciding':
      $attributes = array(
        'class' => 'vbgame_tabelle',
      );
      $header = array(
        t('place'),
        t('team'),
        t('points'),
        t('wins'),
        t('set quotient'),
        t('ball quotient'),
      );
      for ($i = 0; $i != count($content); ++$i) {
        $special = strpos($content[$i]->team, $info->pattern) !== FALSE;
        $rows[] = array(
          'data' => array(
            check_plain($content[$i]->place),
            check_plain($content[$i]->team),
            check_plain($content[$i]->points),
            check_plain($content[$i]->wins),
            check_plain($content[$i]->setQuotient),
            check_plain($content[$i]->ballQuotient),
          ),
          'class' => _vbgame_tr_attributes($i, $special),
        );
      }
      $tab = theme('table', $header, $rows, $attributes);
      break;

    case 'tabelle':
      $attributes = array(
        'class' => 'vbgame_tabelle',
      );

      $header = array(
        '&nbsp;',
        t('team'),
        t('matches played'),
        t('set points'),
        t('ball points'),
      );

      for ($i = 0; $i != count($content); ++$i) {
        $special = strpos($content[$i]->team, $info->pattern) !== FALSE;
        $rows[] = array(
          'data' => array(
            check_plain($content[$i]->platz),
            check_plain($content[$i]->team),
            check_plain($content[$i]->spiele),
            check_plain($content[$i]->plussaetze . ' : ' . $content[$i]->minussaetze),
            check_plain($content[$i]->pluspunkte . ' : ' . $content[$i]->minuspunkte),
          ),
          'class' => _vbgame_tr_attributes($i, $special),
        );
      }
      $tab = theme('table', $header, $rows, $attributes);
      break;

    case 'spielplan':
      $attributes = array(
        'class' => 'vbgame_spielplan',
      );
      // Header.
      $header = array(
        t('date'),
        t('begin'),
        t('home - guest'),
        t('result'),
      );
      // Rows.
      $st = 0;
      foreach ($content as $spieltag) {
        $st++;
        foreach ($spieltag as $game) {
          // Result is either provided as complete string or as parts.
          $result = (!empty($game->result)) ? $game->result : $game->sheim . " : " . $game->sgast;

          $rows[] = array(
            'class' => _vbgame_tr_attributes($st, FALSE),
            'data' => array(
              check_plain($game->datum),
              check_plain($game->beginn),
              check_plain($game->heim . ' - ' . $game->gast),
              check_plain($result),
            ),
          );
        }
      }
      $tab = theme('table', $header, $rows, $attributes);
      break;

    case 'vorschau':
      $attributes = array(
        'class' => 'vbgame_vorschau',
      );
      // Header.
      $header = array(
        t('match id'),
        t('date'),
        t('home - guest'),
        t('arena open / match begin'),
      );
      // Body.
      $i = 0;
      foreach ($content as $game) {
        $rows[] = array(
          'class' => _vbgame_tr_attributes($i++, FALSE),
          'data' => array(
            check_plain($game->nr),
            check_plain($game->datum),
            check_plain($game->heim . " - " . $game->gast),
            check_plain($game->hallenoeffnung . " / " . $game->spielbeginn),
          ),
        );
      }
      $tab = theme('table', $header, $rows, $attributes);
      break;
  }

  $output = '<div class="vbgame_data">';
  $output .= $tab;
  $output .= '</div>';

  return $output;
}

/**
 * Implements theme_vbgame_summary_schedule.
 */
function theme_vbgame_summary_schedule($content) {
  $teamlist = array();

  $attributes = array('class' => 'vbgame_spielplan');
  $header = array(
    t('team'),
    t('date and match begin'),
    t('matchup'),
    t('hall'),
    t('result'),
  );

  if (empty($content)) {
    $rows[] = array(
      'data' => array(
        'colspan' => 6,
        'data' => t('There is currently no schedule available for our teams.'),
      ),
    );
  }
  else {
    $st = 0;
    foreach ($content as $spieltag) {
      $st++;
      foreach ($spieltag as $game) {
        if (!array_key_exists($game->teamid, $teamlist)) {
          $teamlist[$game->teamid] = (object) array(
            'teamid' => $game->teamid,
            'name' => $game->name,
            'backend' => $game->backend,
            'xmlfetchtime' => $game->xmlfetchtime,
          );
        }

        if (strpos($game->heim, $game->pattern) !== FALSE) {
          $match = $game->name . " - " . $game->gast;
        }
        elseif (strpos($game->gast, $game->pattern) !== FALSE) {
          $match = $game->heim . " - " . $game->name;
        }
        else {
          $match = $game->heim . " - " . $game->gast;
        }

        $rows[] = array(
          'class' => ($st & 1) ? 'odd' : 'even',
          'data' => array(
            check_plain($game->name),
            check_plain($game->datum . ' ' . $game->beginn),
            check_plain($match),
            check_plain($game->halle),
            check_plain($game->result),
          ),
        );
      }
    }
  }

  $html = "\n<!-- theme_vbgame_summary_schedule --> \n";
  $html .= '<div class="vbgame_data">';
  $html .= theme('table', $header, $rows, $attributes);
  $html .= '</div>';
  $html .= theme('vbgame_srcinfo', $teamlist);

  return $html;
}


/**
 * Implements theme_vbgame_summarypage_content().
 */
function theme_vbgame_summarypage_content($variables) {
  $type = $variables['type'];
  $content = $variables['content'];

  switch ($type) {
    case 'kurztabelle':
      $i = 0;
      foreach ($content as $row) {
        $rows[] = array(
          'class' => ($i++ & 1) ? 'vbgame_even' : 'vbgame_odd',
          'no_striping' => TRUE,
          'data' => array(
            check_plain($row->name),
            check_plain($row->platz),
            check_plain($row->pluspunkte . " : " . $row->minuspunkte),
          ),
        );
      }
      $header = array(
        t('team'),
        t('place'),
        t('points'),
      );
      $attributes = array('class' => 'vbgame_tabelle');
      $tab = theme('table', $header, $rows, $attributes);
      break;

    case 'vorschau':
      $attributes = array('class' => 'vbgame_spielplan');
      $header = array(
        t('team'),
        t('opponent'),
        t('when'),
        t('where'),
      );

      // Content.
      foreach ($content as $dateset) {
        foreach ($dateset as $row) {
          if ($row->heimspiel) {
            // If we are home team, tell us the guest.
            $team = $row->gast;
          }
          else {
            // If we are guest team, tell us home.
            $team = $row->heim;
          }
          if ($team !== FALSE) {
            $rows[] = array(
              'data' => array(
                check_plain($row->name),
                check_plain($team),
                check_plain($row->datum . ' : ' . $row->beginn),
                $row->heimspiel ? t('home game') : t('away game'),
              ),
              'nowrap' => TRUE,
            );
          }
        }
      }

      $tab = theme('table', $header, $rows, $attributes);
      break;
  }

  $output = '<div class="vbgame_data">';
  $output .= $tab;
  $output .= '</div>';
  return $output;
}

/**
 * Theme the Footer Message.
 *
 * List all Data Sources with last-update timestamp
 */
function theme_vbgame_srcinfo($teaminfolist) {
  $output = '<div align="left" id="vbgame-source-listing" ><br \><br \>';

  if (count($teaminfolist) == 1) {
    $team = $teaminfolist[0];
    $output .= '<p>';
    $output .= t('@teamname data provided by @srcprovidermessage. Last updated on  @timestamp',
      array(
        '@teamname' => $team->name,
        '@srcprovidermessage' => variable_get('vbgame_' . $team->backend . '_footermsg', ''),
        '@timestamp' => $team->xmlfetchtime,
      ));
    $output .= '</p>';
  }
  elseif (count($teaminfolist) > 1) {
    // Make a list.
    $output .= '<p><ul>';

    usort($teaminfolist, "_vbgame_cmp_team_object_name");

    foreach ($teaminfolist as $team) {
      $output .= '<li>';
      $output .= t('@teamname data provided by @srcprovidermessage. Last updated on @timestamp',
        array(
          '@teamname' => $team->name,
          '@srcprovidermessage' => variable_get('vbgame_' . $team->backend . '_footermsg', ''),
          '@timestamp' => $team->xmlfetchtime,
        ));
      $output .= '</li>';
    }
    $output .= '</ul></p>';
  }
  $output .= '</div>';

  return $output;
}

/**
 * Compare function for usort.
 */
function _vbgame_cmp_team_object_name($a, $b) {
  return strcmp($a->name, $b->name);
}

/**
 * Provide css class for alternating table row colors <tr> Tag.
 *
 * Provides different css, if $special is true (highlight row)
 */
function _vbgame_tr_attributes($i = 1, $special = FALSE) {
  return ($special) ? 'special' : '';
}
