
This module provides node types, database-cache and management
interface for displaying personalized match information for
various official volleyball competitions in Germany.

=== BACKGROUND ===

The Volleyball Landesverband Württemberg (VLW) is responsible in
organizing official Volleyball competitions in Württemberg, Germany.

They provide game data via XML, thus enabeling interested Clubs to
display customized score tabs, results, next game announcements and
season schedule.

This module is written for the needs of my local club FV Tübinger Modell,
but since SAMS integration is available, it may be useful for many other
clubs in germany as well.
Features

=== Node Types ===

Team Node Types:

    Teampage Tabelle: Displays the scoretable for a single team.
    Teampage Spielplan: Displays the full game schedule for a single team.

Summary Node Types:

    Gesamtspielplan
    Kurztabelle: Display a table with only our Teams for a given Season.

Teammanager:

The Teammanager is the core of this module.
Here you define which Information is available for our nodes to display.

You are required to enter an Alias for each Team (e.g. "Herren 1"), along with
the information of season, matchseries and official team designation.

=== BACKEND SUPPORT ===

Backend Support (internally named "service") is organized in sub-modules. Currently three backends are
integrated:

== VLW ==
http://www.vlw-online.de/

This module offers access to all VLW leagues. Interface description is here:
http://www.vlw-online.de/index.php?tacoma=webpart.pages.TacomaDynamicPage&navid=6123&coid=6123&cid=1

== SAMS ==
http://wiki.sams-server.de/wiki/Hauptseite

SAMS is a patform for many federal volleyball associations in germany for
scorekeeping and schedule organization. They support

    1.Bundesliga (DVL)
    2.Bundesliga (DVL)
    Dritte Liga (DVV)
    all leagues of SHVV
    all leagues of VRPP
    all leagues of SbVV

You need individual and personal API-Keys to access the data.

=== Coming up ===

Since the DVL/DVV changed to a new score scheme, all XML interfaces will likely
change during August 2013.
