/**
 * Implements hook_node_info().
 *
 * Define our Content Types.
 */
function vbgame_node_info() {
  return array(
    'vbgame_teampage' => array(
      'name' => t('Volleyball Team Page'),
      'base' => 'vbgame_teampage',
      'description' => t("per team rankings and per team schedules."),
    ),
    'vbgame_summarypage' => array(
      'name' => t('Volleyball Summary Page'),
      'base' => 'vbgame_summarypage',
      'description' => t("Summary schedule, ranking overview, ..."),
    ),
  );
}
