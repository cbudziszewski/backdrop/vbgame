<?php
/**
 * @file
 * VLW Module.
 * This Module handles communication with the VLW Data Provider.
 *
 * @link http://www.vlw-online.de/index.php?tacoma=webpart.pages.TacomaDynamicPage&navid=6229&coid=6229&cid=1 Interface description @endlink
 */

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++ VBGAME BACKEND FUNCTIONS ++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**
 * Provides the URL prefix for fetching XML content.
 */
function vbgame_vlw_interface_url() {
  return 'http://www.vlw-online.de/xml/';
}

/**
 * Implements getStaffelOptions hook from vbgame_teammanager.inc.
 *
 * provides Staffel Information, as it is not provided via XML
 *
 * @param string $saison
 *   The season of interest
 */
function vbgame_vlw_getStaffelOptions($service_name, $saison) {
  // @TODO: Mixed.
  $aktive = array(
    "Aktive" => "---------- AKTIVE ----------",
    "10" => "Regionalliga Sued, Damen",
    "71" => "Regionalliga Sued, Herren",
    "14" => "Oberliga, Damen",
    "73" => "Oberliga, Herren",
    "15" => "Landesliga Nord, Damen",
    "16" => "Landesliga Sued, Damen",
    "2" => "Landesliga Nord, Herren",
    "1" => "Landesliga Sued, Herren",
    "Aktive Nord" => "---------- AKTIVE NORD----------",
    "4" => "Bezirksliga Nord, Herren",
    "49" => "A-Klasse 1 Nord, Herren",
    "50" => "A-Klasse 2 Nord, Herren",
    "51" => "B-Klasse 1 Nord, Herren",
    "52" => "B-Klasse 2 Nord, Herren",
    "11" => "Bezirksliga Nord, Damen",
    "17" => "A-Klasse 1 Nord, Damen",
    "18" => "A-Klasse 2 Nord, Damen",
    "19" => "B-Klasse 1 Nord, Damen",
    "20" => "B-Klasse 2 Nord, Damen",
    "21" => "B-Klasse 3 Nord, Damen",
    "Aktive Ost" => "---------- AKTIVE OST----------",
    "54" => "Bezirksliga Ost, Herren",
    "55" => "A-Klasse 1 Ost, Herren",
    "56" => "A-Klasse 2 Ost, Herren",
    "57" => "B-Klasse 1 Ost, Herren",
    "278" => "B-Klasse 2 Ost, Herren",
    "59" => "B-Klasse 3 Ost, Herren",
    "23" => "Bezirksliga Ost, Damen",
    "24" => "A-Klasse 1 Ost, Damen",
    "25" => "A-Klasse 2 Ost, Damen",
    "26" => "B-Klasse 1 Ost, Damen",
    "27" => "B-Klasse 2 Ost, Damen",
    "28" => "B-Klasse 3 Ost, Damen",
    "29" => "B-Klasse 4 Ost, Damen",
    "238" => "B-Klasse 5 Ost, Damen",
    "239" => "B-Klasse 6 Ost, Damen",
    "Aktive Sued" => "---------- AKTIVE SUED----------",
     "6" => "Bezirksliga Sued, Herren",
    "62" => "A-Klasse 1 Sued, Herren",
    "134" => "B-Klasse 1 Sued, Herren",
    "269" => "B-Klasse 2 Sued, Herren",
    "33" => "Bezirksliga Sued, Damen",
    "34" => "A-Klasse 1 Sued, Damen",
    "35" => "A-Klasse 2 Sued, Damen",
    "36" => "B-Klasse 1 Sued, Damen",
    "37" => "B-Klasse 2 Sued, Damen",
    "38" => "B-Klasse 3 Sued, Damen",
    "Aktive West" => "---------- AKTIVE WEST----------",
    "64" => "Bezirksliga West, Herren",
    "65" => "A-Klasse 1 West, Herren",
    "66" => "A-Klasse 2 West, Herren",
    "67" => "B-Klasse 1 West, Herren",
    "68" => "B-Klasse 2 West, Herren",
    "40" => "Bezirksliga West, Damen",
    "41" => "A-Klasse 1 West, Damen",
    "42" => "A-Klasse 2 West, Damen",
    "43" => "B-Klasse 1 West, Damen",
    "136" => "B-Klasse 2 West, Damen",
    "44" => "B-Klasse 3 West, Damen",
    "45" => "B-Klasse 4 West, Damen",
    "276" => "B-Klasse 5 West, Damen",
  );
  $jugend = array(
    "Jugend" => "---------- JUGEND ----------",
    "142" => "Leistungsstaffel U20 maennlich Nord",
    "143" => "Leistungsstaffel U20 maennlich Sued",
    "165" => "Leistungsstaffel U20 weiblich Nord",
    "166" => "Leistungsstaffel U20 weiblich Sued",
    "237" => "Leistungsstaffel U18 maennlich Nord",
    "145" => "Leistungsstaffel U18 maennlich Sued",
    "167" => "Leistungsstaffel U18 weiblich Nord",
    "168" => "Leistungsstaffel U18 weiblich Sued",
    "Jugend Nord" => "---------- JUGEND NORD ----------",
    "141" => "Bezirksstaffel U20 Nord 1 maennlich",
    "150" => "Bezirksstaffel U18 Nord 1 maennlich",
    "277" => "Bezirksstaffel U16 Nord 1 maennlich",
    "169" => "Bezirksstaffel U20 Nord 1 weiblich",
    "170" => "Bezirksstaffel U20 Nord 2 weiblich",
    "235" => "Bezirksstaffel U20 Nord 3 weiblich",
    "270" => "Bezirksstaffel U20 Nord 4 weiblich",
    "171" => "Bezirksstaffel U18 Nord 1 weiblich",
    "172" => "Bezirksstaffel U18 Nord 2 weiblich",
    "173" => "Bezirksstaffel U18 Nord 3 weiblich",
    "174" => "Bezirksstaffel U16 Nord 1 weiblich",
    "Jugend Ost" => "---------- JUGEND OST ----------",
    "229" => "Bezirksstaffel U20 Ost 1 maennlich",
    "230" => "Bezirksstaffel U20 Ost 2 maennlich",
    "152" => "Bezirksstaffel U18 Ost 1 maennlich",
    "153" => "Bezirksstaffel U18 Ost 2 maennlich",
    "176" => "Bezirksstaffel U20 Ost 1 weiblich",
    "177" => "Bezirksstaffel U20 Ost 2 weiblich",
    "178" => "Bezirksstaffel U20 Ost 3 weiblich",
    "179" => "Bezirksstaffel U20 Ost 4 weiblich",
    "180" => "Bezirksstaffel U20 Ost 5 weiblich",
    "181" => "Bezirksstaffel U18 Ost 1 weiblich",
    "182" => "Bezirksstaffel U18 Ost 2 weiblich",
    "183" => "Bezirksstaffel U18 Ost 3 weiblich",
    "186" => "Bezirksstaffel U16 Ost 1 weiblich",
    "279" => "Bezirksstaffel U16 Ost 2 weiblich",
    "Jugend Sued" => "---------- JUGEND SUED ----------",
    "148" => "Bezirksstaffel U20 Sued 1 maennlich",
    "154" => "Bezirksstaffel U18 Sued 2 maennlich",
    "162" => "Bezirksstaffel U16 Sued 2 maennlich",
    "220" => "Bezirksstaffel U15 Sued 1 maennlich",
    "221" => "Bezirksstaffel U15 Sued 2 maennlich",
    "189" => "Bezirksstaffel U20 Sued 1 weiblich",
    "190" => "Bezirksstaffel U20 Sued 2 weiblich",
    "251" => "Bezirksstaffel U20 Sued 3 weiblich",
    "191" => "Bezirksstaffel U18 Sued 1 weiblich",
    "192" => "Bezirksstaffel U18 Sued 2 weiblich",
    "193" => "Bezirksstaffel U18 Sued 3 weiblich",
    "194" => "Bezirksstaffel U16 Sued 1 weiblich",
    "Jugend West" => "---------- JUGEND WEST ----------",
    "147" => "Bezirksstaffel U20 West 1 maennlich",
    "232" => "Bezirksstaffel U20 West 2 maennlich",
    "156" => "Bezirksstaffel U18 West 1 maennlich",
    "164" => "Bezirksstaffel U16 West 1 maennlich",
    "197" => "Bezirksstaffel U20 West 1 weiblich",
    "198" => "Bezirksstaffel U20 West 2 weiblich",
    "199" => "Bezirksstaffel U20 West 3 weiblich",
    "258" => "Bezirksstaffel U20 West 4 weiblich",
    "200" => "Bezirksstaffel U18 West 1 weiblich",
    "201" => "Bezirksstaffel U18 West 2 weiblich",
    "264" => "Bezirksstaffel U18 West 3 weiblich",
    "202" => "Bezirksstaffel U16 West 1 weiblich",
    "293" => "Bezirksstaffel U16 West 2 weiblich",
  );

  // Record Season changes here.
  switch ($saison) {
    case 2013:
    default:
  }

  switch ($service_name) {
    case 'vlw_aktive':
      return $aktive;

    case 'vlw_jugend':
      return $jugend;

    default:
      return array();
  }
}

/**
 * Validate Form Element Staffel.
 *
 * Callback for Teammanager
 */
function vbgame_vlw_validate_form_element_staffel($service_name, $value) {
  // Make sure we don't select our spacer!
  return is_numeric($value);
}
/**
 * Honor update restrictions.
 */
function vbgame_vlw_check_should_update($service_name, $teaminfo) {
  // We currently have no restrictions.
  return TRUE;
}

/**
 * Implements getTeamOptions hook from vbgame_teammanager.inc.
 *
 * @param string $saison
 *   The season-code for the season of interest
 * @param string $sid
 *   staffel-ID for the staffel of interest
 */
function vbgame_vlw_getTeamOptions($service_name, $saison, $sid) {
  $encoding = 'iso-8859-15';

  $url = vbgame_vlw_interface_url() . 'tabelle_' . $saison . '_' . $sid . '.xml';

  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);

  // In einzelne Datensaeze aufsplitten.
  preg_match_all("|<element>(.*)</element>|Usim", $content, $elemente, PREG_PATTERN_ORDER);

  // Extract content!
  $elemente = $elemente[0];
  for ($i = 0; $i != count($elemente); ++$i) {
    $teamname = check_plain(vbgame_extract_xml_single('team', $elemente[$i]));
    $r[$teamname] = $teamname;
  }
  return $r;
}

/*
 * Implements vbgame_vlw_get_default_rankingtype().
 */
function vbgame_vlw_get_default_rankingtype($service_name, $season, $staffel = NULL) {
  if ($season <= 2013) {
    return 'twopoint';
  }

  switch ($service_name) {
    case 'vlw_aktive':
      return 'threepoint';

    case 'vlw_jugend':
      return 'threepoint_twoset';
  }
}

/**
 * Implements vlw_fetch_all().
 *
 * retrieve remote content and return in structured form.
 */
function vbgame_vlw_fetch_all($service_name, $teaminfo) {
  if (strncmp($service_name, 'vlw', 3) !== 0) {
    // Just fail silently, if wrong service!
    return;
  }
  // Fetch new content for team.
  $content['spielplan'] = vbgame_vlw_load_remote_spielplan($teaminfo);

  switch ($teaminfo->rankingtype) {
    case 'twopoint':
      $content['ranking'] = vbgame_vlw_load_remote_tabelle($teaminfo);
      break;

    case 'threepoint':
      $content['ranking'] = vbgame_vlw_load_remote_tabelle_threepoint($teaminfo);
      break;

    case 'threepoint-twoset':
      $content['ranking'] = vbgame_vlw_load_remote_tabelle_threepoint_twoset($teaminfo);
      break;
  }

  return $content;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++ VLW REMOTE FETCH FUNCTIONS ++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++


/**
 * Implements vbgame_vlw_load_remote_spielplan().
 *
 * fetches/updates Season Schedules and Results
 *
 * @param array $teaminfo
 *   The team information struct
 */
function vbgame_vlw_load_remote_spielplan($teaminfo) {
  $encoding = 'iso-8859-15';

  $sid = $teaminfo->sid;
  $saison = $teaminfo->jahr;

  $url = vbgame_vlw_interface_url() . 'spielplan_' . $saison . '_' . $sid . '.xml';

  // Fetch.
  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  // Match.
  preg_match_all("|<element>(.*)</element>|Usim", $content, $elemente, PREG_PATTERN_ORDER);
  $elemente = $elemente[0];

  // Create Table.
  for ($i = 0; $i != count($elemente); ++$i) {
    $r[$i]['nr'] = vbgame_extract_xml_single('nr', $elemente[$i]);
    $r[$i]['spieltag'] = vbgame_extract_xml_single('spieltag', $elemente[$i]);
    $r[$i]['datum'] = vbgame_extract_xml_single('datum', $elemente[$i]);
    $r[$i]['beginn'] = vbgame_extract_xml_single('beginn', $elemente[$i]);
    $r[$i]['heim'] = vbgame_extract_xml_single('heim', $elemente[$i]);
    $r[$i]['gast'] = vbgame_extract_xml_single('gast', $elemente[$i]);
    $r[$i]['sheim'] = vbgame_extract_xml_single('sheim', $elemente[$i]);
    $r[$i]['sgast'] = vbgame_extract_xml_single('sgast', $elemente[$i]);
    $r[$i]['halle'] = vbgame_extract_xml_single('halle', $elemente[$i]);
    $r[$i]['result'] = vbgame_extract_xml_single('result', $elemente[$i]);
  }

  return $r;
}

/**
 * Implements vbgame_vlw_load_remote_tabelle() for updateXMLCache().
 *
 * fetches/updates Ranking
 *
 * @param array $teaminfo
 *   The team information struct
 */
function vbgame_vlw_load_remote_tabelle($teaminfo) {
  $encoding = 'iso-8859-15';

  $staffel = $teaminfo->sid;
  $saison = $teaminfo->jahr;

  $url = vbgame_vlw_interface_url() . 'tabelle_' . $saison . '_' . $staffel . '.xml';

  // Fetch.
  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  // Match.
  preg_match_all("|<element>(.*)</element>|Usim", $content, $elemente, PREG_PATTERN_ORDER);
  $elemente = $elemente[0];

  // Create Table.
  for ($i = 0; $i != count($elemente); ++$i) {
    $tabelle[$i]['platz'] = vbgame_extract_xml_single('platz', $elemente[$i]);
    $tabelle[$i]['team'] = vbgame_extract_xml_single('team', $elemente[$i]);
    $tabelle[$i]['spiele'] = vbgame_extract_xml_single('spiele', $elemente[$i]);
    $tabelle[$i]['pluspunkte'] = vbgame_extract_xml_single('pluspunkte', $elemente[$i]);
    $tabelle[$i]['minuspunkte'] = vbgame_extract_xml_single('minuspunkte', $elemente[$i]);
    $tabelle[$i]['plussaetze'] = vbgame_extract_xml_single('plussaetze', $elemente[$i]);
    $tabelle[$i]['minussaetze'] = vbgame_extract_xml_single('minussaetze', $elemente[$i]);
    $tabelle[$i]['plusbaelle'] = vbgame_extract_xml_single('plusbaelle', $elemente[$i]);
    $tabelle[$i]['minusbaelle'] = vbgame_extract_xml_single('minusbaelle', $elemente[$i]);
  }

  return $tabelle;
}
/**
 * Implements vbgame_vlw_load_remote_tabelle_threepoint().
 *
 * fetches/updates Ranking for threepoint rule
 *
 * @param array $teaminfo
 *   The team information struct
 */
function vbgame_vlw_load_remote_tabelle_threepoint($teaminfo) {
  $encoding = 'iso-8859-15';

  $staffel = $teaminfo->sid;
  $saison = $teaminfo->jahr;

  $url = vbgame_vlw_interface_url() . 'tabelle_' . $saison . '_' . $staffel . '.xml';

  // Fetch.
  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  // Match.
  preg_match_all("|<element>(.*)</element>|Usim", $content, $elemente, PREG_PATTERN_ORDER);
  $elemente = $elemente[0];

  // Create Table.
  for ($i = 0; $i != count($elemente); ++$i) {
    $tabelle[$i]['team'] = vbgame_extract_xml_single('team', $elemente[$i]);
    $tabelle[$i]['matchesPlayed'] = vbgame_extract_xml_single('spiele', $elemente[$i]);

    $plussaetze = vbgame_extract_xml_single('plussaetze', $elemente[$i]);
    $minussaetze = vbgame_extract_xml_single('minussaetze', $elemente[$i]);
    $tabelle[$i]['setWinScore'] = $plussaetze;
    $tabelle[$i]['setLoseScore'] = $minussaetze;
    $tabelle[$i]['setPoints'] = implode(":", array($plussaetze, $minussaetze));
    $tabelle[$i]['setPointDifference'] = (int) $plussaetze - (int) $minussaetze;
    $tabelle[$i]['setQuotient'] = sprintf('%5.2f', ((int) $plussaetze / (int) $minussaetze));
    unset($plussaetze);
    unset($minussaetze);

    $plusbaelle = vbgame_extract_xml_single('plusbaelle', $elemente[$i]);
    $minusbaelle = vbgame_extract_xml_single('minusbaelle', $elemente[$i]);
    $tabelle[$i]['ballWinScore'] = $plusbaelle;
    $tabelle[$i]['ballLoseScore'] = $minusbaelle;
    $tabelle[$i]['ballPoints'] = implode(":", array($plusbaelle, $minusbaelle));
    $tabelle[$i]['ballPointDifference'] = (int) $plusbaelle - (int) $minusbaelle;
    $tabelle[$i]['ballQuotient'] = sprintf('%5.2f', ((int) $plusbaelle / (int) $minusbaelle));
    unset($plusbaelle);
    unset($minusbaelle);

    $tabelle[$i]['points'] = vbgame_extract_xml_single('dppunkte', $elemente[$i]);
    $tabelle[$i]['place'] = vbgame_extract_xml_single('dpplatz', $elemente[$i]);
    $tabelle[$i]['wins'] = vbgame_extract_xml_single('dpsiege', $elemente[$i]);
    $tabelle[$i]['losses'] = vbgame_extract_xml_single('dpniederlagen', $elemente[$i]);

    $tabelle[$i]['count30'] = vbgame_extract_xml_single('dpgewinn3031', $elemente[$i]);
    $tabelle[$i]['count31'] = NULL;
    $tabelle[$i]['count32'] = vbgame_extract_xml_single('dpgewinn32', $elemente[$i]);
    $tabelle[$i]['count23'] = vbgame_extract_xml_single('dpniederlage23', $elemente[$i]);
    $tabelle[$i]['count13'] = NULL;
    $tabelle[$i]['count03'] = vbgame_extract_xml_single('dpniederlage1303', $elemente[$i]);
  }

  return $tabelle;
}
/**
 * Implements vbgame_vlw_load_remote_tabelle_threepoint_twoset().
 *
 * fetches/updates Ranking for threepoint twoset rule
 *
 * @param array $teaminfo
 *   The team information struct
 */
function vbgame_vlw_load_remote_tabelle_threepoint_twoset($teaminfo) {
  $encoding = 'iso-8859-15';

  $staffel = $teaminfo->sid;
  $saison = $teaminfo->jahr;

  $url = vbgame_vlw_interface_url() . 'tabelle_' . $saison . '_' . $staffel . '.xml';

  // Fetch.
  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  // Match.
  preg_match_all("|<element>(.*)</element>|Usim", $content, $elemente, PREG_PATTERN_ORDER);
  $elemente = $elemente[0];

  // Create Table.
  for ($i = 0; $i != count($elemente); ++$i) {
    $tabelle[$i]['team'] = vbgame_extract_xml_single('team', $elemente[$i]);
    $tabelle[$i]['matchesPlayed'] = vbgame_extract_xml_single('spiele', $elemente[$i]);

    $plussaetze = vbgame_extract_xml_single('plussaetze', $elemente[$i]);
    $minussaetze = vbgame_extract_xml_single('minussaetze', $elemente[$i]);
    $tabelle[$i]['setWinScore'] = $plussaetze;
    $tabelle[$i]['setLoseScore'] = $minussaetze;
    $tabelle[$i]['setPoints'] = implode(":", array($plussaetze, $minussaetze));
    $tabelle[$i]['setPointDifference'] = (int) $plussaetze - (int) $minussaetze;
    $tabelle[$i]['setQuotient'] = sprintf('%5.2f', ((int) $plussaetze / (int) $minussaetze));
    unset($plussaetze);
    unset($minussaetze);

    $plusbaelle = vbgame_extract_xml_single('plusbaelle', $elemente[$i]);
    $minusbaelle = vbgame_extract_xml_single('minusbaelle', $elemente[$i]);
    $tabelle[$i]['ballWinScore'] = $plusbaelle;
    $tabelle[$i]['ballLoseScore'] = $minusbaelle;
    $tabelle[$i]['ballPoints'] = implode(":", array($plusbaelle, $minusbaelle));
    $tabelle[$i]['ballPointDifference'] = (int) $plusbaelle - (int) $minusbaelle;
    $tabelle[$i]['ballQuotient'] = sprintf('%5.2f', ((int) $plusbaelle / (int) $minusbaelle));
    unset($plusbaelle);
    unset($minusbaelle);

    $tabelle[$i]['points'] = vbgame_extract_xml_single('dppunkte', $elemente[$i]);
    $tabelle[$i]['place'] = vbgame_extract_xml_single('dpplatz', $elemente[$i]);
    $tabelle[$i]['wins'] = vbgame_extract_xml_single('dpsiege', $elemente[$i]);
    $tabelle[$i]['losses'] = vbgame_extract_xml_single('dpniederlagen', $elemente[$i]);

    $tabelle[$i]['count30'] = vbgame_extract_xml_single('dpgewinn20', $elemente[$i]);
    $tabelle[$i]['count31'] = NULL;
    $tabelle[$i]['count32'] = vbgame_extract_xml_single('dpgewinn21', $elemente[$i]);
    $tabelle[$i]['count23'] = vbgame_extract_xml_single('dpniederlage12', $elemente[$i]);
    $tabelle[$i]['count13'] = NULL;
    $tabelle[$i]['count03'] = vbgame_extract_xml_single('dpniederlage02', $elemente[$i]);
  }

  return $tabelle;
}
