<?php
/**
 * @file
 * Part of vbgame Module
 * This inc provides the Teammanager functionality
 */

/**
 * Implements hook_form().
 *
 * Teammanager
 *
 * This form is used to display, add and edit the team database.
 */
function vbgame_teammanager_default_form() {
  $form = array();
  $form['table'] = vbgame_teammanager_form_tabelle();
  $form['updatexml'] = array(
    '#type' => 'submit',
    '#value' => 'Update XML Team',
    '#submit' => array('vbgame_teammanager_form_updatexml_submit'),
    '#validate' => array('vbgame_teammanager_form_check_team_selected'),
  );
  $form['teamedit'] = array(
    '#type' => 'submit',
    '#value' => 'Edit Team information',
    '#submit' => array('vbgame_teammanager_form_edit'),
    '#validate' => array('vbgame_teammanager_form_check_team_selected'),
  );
  return $form;
}
/**
 * This function generates the team table.
 */
function vbgame_teammanager_form_tabelle() {

  $resource = db_query("SELECT * FROM {vbgame_teamtable} ORDER BY jahr, backend, weight");

  $header = array(
    'teamid' => t('teamID'),
    'name' => t('display name'),
    'pattern' => t('pattern'),
    'sid' => t('match series id'),
    'jahr' => t('season'),
    'weight' => t('weight'),
    'backend' => t('backend'),
    'rankingtype' => t('ranking type'),
    'remoteid' => t('remote teamid'),
    'timestamp' => t('xml timestamp'),
  );

  $options = array();
  foreach ($resource as $team) {
    $options[$team->teamid] = array(
      'teamid' => $team->teamid,
      'name' => check_plain($team->name),
      'pattern' => check_plain($team->pattern),
      'sid' => check_plain($team->sid),
      'jahr' => check_plain($team->jahr),
      'weight' => check_plain($team->weight),
      'backend' => check_plain($team->backend),
      'rankingtype' => check_plain($team->rankingtype),
      'remoteid' => check_plain($team->remoteID),
      'timestamp' => check_plain($team->xmlfetchtime),
    );
  }
  // Using <tableselect> from elements module.
  $table = array(
    '#type' => 'tableselect',
    '#multiple' => FALSE,
    '#empty' => t('There are no teams in the database.'),
    '#header' => $header,
    '#options' => $options,
  );
  return $table;
}

/**
 * Multi step wizard to add a team to the database.
 */
function vbgame_teammanager_add_form($form, $form_state) {
print_r($form);
print_r($form_state);

  if (!isset($form_state['storage'])) {
    $form_state['storage']['step'] = 1;
  }
  $form_state['values'] = $form_state['storage']['values'];
  drupal_set_title(t('Add Team Step @step of @totalsteps', array(
    '@step' => $form_state['storage']['step'],
    '@totalsteps' => 3,
  )), PASS_THROUGH);

  $step = $form_state['storage']['step'];

  $service_list = config_get('vbgame.settings','backend_service_list');

  $form = array();

  switch ($step) {
    case 3:
      $service_name = $form_state['values']['backend'];
      $service_module = $service_list[$service_name]['module'];

      $teamlist = call_user_func(
        implode("_", array($service_module, 'getTeamOptions')),
        $service_name,
        $form_state['values']['saison'],
        $form_state['values']['staffel']
      );

      $form['team'] = array(
        '#title' => 'Select Team',
        '#type' => 'select',
        '#options' => $teamlist,
        '#default_value' => $form_state['values']['team'],
        '#weight' => 3,
        '#required' => TRUE,
      );

    case 2:
      $service_name = $form_state['values']['backend'];
      $service_module = $service_list[$service_name]['module'];

      $staffellist = call_user_func(
        implode("_", array($service_module, 'getStaffelOptions')),
        $service_name,
        $form_state['values']['saison']
      );

      $form['staffel'] = array(
        '#title' => 'Staffel',
        '#type' => 'select',
        '#options' => $staffellist,
        '#default_value' => $form_state['values']['staffel'],
        '#element_validate' => array('vbgame_teammanager_add_form_element_validate'),
        '#weight' => 2,
        '#required' => TRUE,
      );

    case 1:
      $form['backend'] = array(
        '#title' => 'Backend',
        '#type' => 'select',
        '#default_value' => $form_state['values']['backend'],
        '#options' => vbgame_get_options('backends_enabled'),
        '#size' => 1,
        '#weight' => -1,
        '#required' => TRUE,
      );

      $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Team Name'),
        '#default_value' => $form_state['values']['name'],
        '#size' => 25,
        '#maxlength' => 200,
        '#description' => t("Meaningfull Team Name"),
        '#weight' => 1,
        '#element_validate' => array('vbgame_teammanager_add_form_element_validate'),
        '#required' => TRUE,
      );

      $form['saison'] = array(
        '#type' => 'select',
        '#title' => t('Season'),
        '#default_value' => $form_state['values']['saison'],
        '#options' => vbgame_get_options('season'),
        '#size' => 1,
        '#weight' => 1,
        '#element_validate' => array('vbgame_teammanager_add_form_element_validate'),
        '#required' => TRUE,
      );
  }
  // Corresponds to 'edit-back' in form_submit
  $form['back'] = array(
    '#value' => 'back',
    '#type' => 'submit',
    '#weight' => 6,
  );
  // Corresponds to 'edit-next' in form_submit.
  $form['next'] = array(
    '#type' => 'submit',
    '#value' => 'next',
    '#weight' => 7,
  );

  if ($step == 1) {
    $form['back']['#disabled'] = TRUE;
  }

  if ($step > 1) {
    $form['backend']['#disabled'] = TRUE;
  }
  if ($step > 2) {
    $form['staffel']['#disabled'] = TRUE;
    $form['saison']['#disabled'] = TRUE;
    $form['next']['#value'] = t('finish');
  }

  return $form;
}

/**
 * Validate function for add-wizard.
 */
function vbgame_teammanager_add_form_validate($form, &$form_state) {
  if (!$form_state['storage']['values']) {
    $form_state['storage']['values'] = array();
  }
  $values = $form_state['storage']['values'] + $form_state['values'];
  $active_season = config_get('vbgame.settings', 'active_season');
  if ($active_season != $values['saison']) {
    $message = t('Saison Setting is @season ! You will not get any content with XML-Update!',
      array('@season' => $active_season));
    drupal_set_message($message, 'warning');
  }
}

/**
 * Validate elements from team-manager form.
 */
function vbgame_teammanager_add_form_element_validate($element, &$form_state) {
  $service_list = config_get('vbgame.settings', 'backend_service_list');

  switch ($element['#name']) {
    case 'staffel':
      $service_name = $form_state['values']['backend'];
      $service_module = $service_list[$service_name]['module'];

      $ok = call_user_func(
        implode("_", array($service_module, 'validate_form_element_staffel')),
        $service_name,
        $element['#value']
      );
      if (!$ok) {
        form_set_error($element, t('Please select a valid match series!'));
      }
      break;
  }
}

/**
 * Form submit function for multistep team add wizard.
 */
function vbgame_teammanager_add_form_submit($form, &$form_state) {
  // Multistep processing.
  if (!$form_state['storage']['values']) {
    $form_state['storage']['values'] = array();
  }
  // Increment the button, overwrite the storage values with the new values,
  // and return.
  if ($form_state['clicked_button']['#id'] == 'edit-back') {
    $form_state['storage']['step']--;
    $form_state['storage']['values'] = $form_state['values'] + $form_state['storage']['values'];
  }
  elseif ($form_state['clicked_button']['#id'] == 'edit-next') {
    $form_state['storage']['step']++;
    $form_state['storage']['values'] = $form_state['values'] + $form_state['storage']['values'];
  }

  if ($form_state['storage']['step'] > 3) {
    drupal_set_message(t('Team Added'));
    $form_state['redirect'] = 'admin/content/vbgame/tm';

    // If this is the last step,
    // clear the storage and rebuild so that the fom doesn't rebuild,
    // and the #redirect works.
    $values = $form_state['storage']['values'] + $form_state['values'];

    $object = new stdClass();
    $object->name = check_plain($values['name']);
    $object->pattern = check_plain(trim($values['team']));
    $object->sid = check_plain($values['staffel']);
    $object->jahr = check_plain($values['saison']);
    $object->backend = check_plain($values['backend']);
    $object->rankingtype = 'threepoint';
    $object->remoteID = NULL;
    $object->weight = 1;

    $ret = drupal_write_record('vbgame_teamtable', $object);

    if ($ret == SAVED_NEW) {
      drupal_set_message(t('Team successfully added to Database. Use manual update to fetch initial content!'));
    }
    else {
      form_set_error('', t('Team INSERT failed!'));
    }

    $form_state['rebuild'] = TRUE;
    unset($form_state['storage']);
  }
}

/**
 * This form lets you edit a selected Team.
 */
function vbgame_teammanager_form_edit($form, &$form_state) {
  // ToDo: redo edit form like add form.
  $form['table'] = vbgame_teammanager_form_tabelle();
  $teamid = $form['table']['#value'];
  drupal_set_message('Edit Team !teamid', array('!teamid' => $teamid));

  $form = array();
  $form['actions'] = array('#type' => 'actions');

  $form_state['values'] = $form_state['storage']['values'];

  $form['newname'] = array(
    '#type' => 'textfield',
    '#title' => 'Team display name',
    '#description' => t("Team display name is a custom team name."),
    '#default_value' => '',
    '#size' => 25,
    '#maxlength' => 200,
    '#required' => FALSE);

  $form['newpattern'] = array(
    '#type' => 'textfield',
    '#title' => 'Highlight regex pattern',
    '#description' => t("regex pattern for highlighting"),
    '#default_value' => '',
    '#size' => 25,
    '#maxlength' => 200,
    '#required' => FALSE);

  $form['newweight'] = array(
    '#type' => 'weight',
    '#title' => 'weight',
    '#default_value' => 0,
  );

  $form['actions']['edit'] = array(
    '#type' => 'submit',
    '#value' => t('update team information'),
    '#submit' => array('vbgame_teammanager_form_edit_submit'),
    '#validate' => array('vbgame_teammanager_form_edit_validate'),
  );

  return $form;
}

/**
 * This form lets you delete a selected Team.
 */
function vbgame_teammanager_form_delete() {
  $form = array();
  $form['table'] = vbgame_teammanager_form_tabelle();
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Team'),
    '#submit' => array('vbgame_teammanager_form_delete_submit'),
    '#validate' => array('vbgame_teammanager_form_check_team_selected'),
  );
  return $form;
}

/**
 * Validate Team select.
 *
 * checks, if you have selected a team.
 */
function vbgame_teammanager_form_check_team_selected($form, &$form_state) {
  if (empty($form['table']['#value'])) {
    form_set_error('table', t('Select a Team!'));
    return;
  }
}

/**
 * Team delete Function.
 */
function vbgame_teammanager_form_delete_submit($form, &$form_state) {
  $teamid = $form['table']['#value'];
  // Delete associated content first.
  vbgame_db_delete_team_from('vbgame_tabelle', $teamid);
  vbgame_db_delete_team_from('vbgame_spielplan', $teamid);
  // Delete Teamtable entry.
  $ret = db_query("DELETE FROM {vbgame_teamtable} WHERE teamid = %d", $teamid);
  if (db_affected_rows() == 1) {
    drupal_set_message(t('Team DELETE successful!'));
  }
  else {
    form_set_error('', t('Team DELETE failed! @RET',
      array('@RET' => $ret)));
  }
}

/**
 * Implements form_insert_validate().
 *
 * Teammanager
 */
function vbgame_teammanager_form_insert_validate($form, &$form_state) {
  // Pattern must be unique per match series!
  $query = db_select('vbgame_teamtable', 't')
    ->fields('1')
    ->condition('t.pattern', $form['tm_new']['pattern']['#value'], '=')
    ->condition('t.jahr', $form['tm_new']['saison']['#value'], '=')
    ->condition('t.sid', $form['tm_new']['staffel']['#value'], '=');

  $has_rows = (bool) db_query_range($query, 0, 1)->fetchField();
  if ($has_rows) {
    form_set_error('tm_new', t('Pattern not unique in this match series: @staffel',
      array('@staffel' => $form['tm_new']['staffel']['#value'])));
    return;
  }

  // Team Display Name should be unique
  // (not really a requirement, but it does not make much sense to me).
  $query = db_select('vbgame_teamtable', 't')
    ->fields('1')
    ->condition('t.name', $form['tm_new']['name']['#value'], '=')
    ->condition('t.jahr', $form['tm_new']['saison']['#value'], '=');

  $has_rows = (bool) db_query_range($query, 0, 1)->fetchField();
  if ($has_rows) {
    form_set_error('tm_new][name', t('Team Name already used in this season: @saison',
      array('@saison' => $form['tm_new']['saison']['#value'])));
    return;
  }
}

/**
 * Implements form_edit_validate().
 *
 * Teammanager
 */
function vbgame_teammanager_form_edit_validate($form, &$form_state) {
  // Team selected?
  if (empty($form['table']['#value'])) {
    form_set_error('table', t('Select a Team!'));
    return;
  }

  // Same restrictions as in insert apply!
  vbgame_teammanager_form_insert_validate($form, $form_state);
}
/**
 * Implements form_edit_submit().
 *
 * Teammanager
 */
function vbgame_teammanager_form_edit_submit($form, &$form_state) {
  // Check for empty values!
  drupal_set_message(t('Team UPDATE called!'));
  $teamid = $form['table']['#value'];

  db_query("UPDATE {vbgame_teamtable} SET name = '%s', pattern = '%s', sid = '%s', jahr = '%s', weight = %d WHERE teamid = %d",
    $form['tm_new']['name']['#value'],
    $form['tm_new']['pattern']['#value'],
    $form['tm_new']['staffel']['#value'],
    $form['tm_new']['saison']['#value'],
    $form['tm_new']['weight']['#value'],
    $teamid
  );

  if (db_affected_rows() == 1) {
    drupal_set_message(t('Team UPDATE successful!'));
  }
  else {
    form_set_error('', t('Team UPDATE failed!'));
  }
}
/**
 * Implements form_updatexml_submit().
 *
 * Teammanager
 */
function vbgame_teammanager_form_updatexml_submit($form, &$form_state) {
  drupal_set_message(t('Team XML UPDATE called!'));
  $teamid = $form['table']['#value'];
  vbgame_team_update_all($teamid, TRUE, NULL);
}
