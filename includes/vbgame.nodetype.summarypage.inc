// +++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++ NODETYPE Summarypage Functions ++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 * Implements hook_form().
 *
 * Nodetype Summarypage
 */
function vbgame_summarypage_form($node, &$form_state) {
  $type = node_type_get_name($node);
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#default_value' => !empty($node->title) ? $node->title : check_plain($type->title_label),
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['teaser'] = array(
    '#type' => 'textarea',
    '#title' => 'Teaser Text',
    '#rows' => 3,
    '#required' => TRUE,
    '#default_value' => !empty($node->teaser) ? $node->teaser : check_plain($type->name),
  );

  $form['body'] = array(
    '#type' => 'hidden',
    '#title' => 'BODY Text',
    '#rows' => 3,
    '#required' => FALSE,
    '#default_value' => '<em>' . t('Table is auto-generated.') . '</em>',
  );

  $form['vbgame_type'] = array(
    '#type' => 'select',
    '#title' => t('page type'),
    '#options' => vbgame_get_options('summary_page_types'),
    '#default_value' => !empty($node->vbgame_type) ? $node->vbgame_type : '',
    '#size' => 1,
    '#required' => TRUE);

  $form['vbgame_saison'] = array(
    '#type' => 'select',
    '#title' => t('season'),
    '#options' => vbgame_get_options('season'),
    '#default_value' => !empty($node->vbgame_saison) ? $node->vbgame_saison : '',
    '#size' => 1,
    '#required' => TRUE);
  return $form;
}
/**
 * Implements hook_insert().
 *
 * Nodetype Summarypage
 */
function vbgame_summarypage_insert($node) {
  $object = new stdClass();
  $object->nid = $node->nid;
  $object->type = $node->vbgame_type;
  $object->saison = $node->vbgame_saison;

  drupal_write_record('vbgame_summarypage', $object);
}
/**
 * Implements hook_update().
 *
 * Nodetype Summarypage
 */
function vbgame_summarypage_update($node) {
  $object = new stdClass();
  $object->nid = $node->nid;
  $object->type = $node->vbgame_type;
  $object->saison = $node->vbgame_saison;

  drupal_write_record('vbgame_summarypage', $object, 'nid');
}
/**
 * Implements hook_delete().
 *
 * Nodetype Summarypage
 */
function vbgame_summarypage_delete($node) {
  db_query('DELETE FROM {vbgame_summarypage} WHERE nid = %d', $node->nid);
}
/**
 * Implements hook_load().
 *
 * Nodetype Summarypage
 */
function vbgame_summarypage_load($node) {
  $additions = db_query('SELECT type as vbgame_type, saison as vbgame_saison FROM {vbgame_summarypage} WHERE nid = :nid',
    array(':nid' => $node->nid));
  return $additions;
}
/**
 * Implements hook_view().
 *
 * Nodetype Summarypage
 */
function vbgame_summarypage_view($node, $view_mode) {

  if ($view_mode == 'teaser') {
    $node->readmore = TRUE;
    $node->teaser = check_markup($node->teaser, $node->format);
    $node->content['body'] = array(
      '#value' => $node->teaser,
      '#weight' => 0,
    );
    return $node;
  }

  switch ($node->vbgame_type) {
    case 'spielplan':
      $node->content['body'] = array(
        '#value' => theme('vbgame_summary_schedule',
          vbgame_summary_content('spielplan', $node->vbgame_saison)),
        '#weight' => 1,
      );
      break;

    default:
      $node->content['body'] = array(
        '#value' => theme('vbgame_summarypage_content', array(
          'type' => $node->vbgame_type,
          'content' => vbgame_summary_content('spielplan', $node->vbgame_saison),
          )),
        '#weight' => 1,
      );
  }

  return $node;
}

