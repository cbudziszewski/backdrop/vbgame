<?php
/**
 * @file
 * DVL Module.
 * This Module handles communication with the SAMS data provider.
 *
 * SAMS handles
 *   - 1. and 2. Bundesliga of DVL,
 *   - Dritte Ligen of DVV,
 *   - all Leagues of SHVV,
 *   - all Leagues of VVRP,
 *   - all Leagues of SbVV.
 *
 * For proper communication with SAMS, you need your own SAMS Api Key
 * look at
 * @link http://www.wiki.sams-server.de/wiki/XML-Schnittstelle SAMS Wiki @endlink
 * for further information!
 *
 * XML Parsing is done by simplexml_load_string().
 */

/**
 * Implements hook_menu().
 */
function vbgame_sams_menu() {
  $items = array();
  // Module Settings.
  $items['admin/settings/vbgame/sams'] = array(
    'title' => 'SAMS XML-Service',
    'weight' => 1,
    'description' => 'Basic settings for SAMS Service pages',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vbgame_sams_settings_form'),
    'access arguments' => array('administer vbgame configuration'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_settings_form_validate().
 */
function vbgame_sams_settings_form_validate($form, &$form_state) {
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++ VBGAME BACKEND FUNCTIONS ++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 * Provides the URL prefix for fetching XML content.
 */
function vbgame_sams_interface_url($service_name) {
  switch ($service_name) {
    case 'sams_DVL':
      return 'http://dvl.sams-server.de/';

    case 'sams_DVV':
      return 'http://dvv.sams-server.de/';

    case 'sams_SHVV':
      return 'http://shvv.sams-server.de/';

    case 'sams_VRPP':
      return 'http://www.vvrp.de/';

    case 'sams_SbVV':
      return 'http://www.sbvv-online.de/';
  }
}

/**
 * Provides access to API-Keys.
 */
function vbgame_sams_getApiKey($service_name) {
  $key = config_get('vbgame.settings','sams_apikey_' . $service_name, '');
  return $key;
}


/**
 * Implements getStaffelOptions().
 *
 * Callback for sams_* backends.
 */
function vbgame_sams_getStaffelOptions($service_name, $saison) {
  $season_id = vbgame_sams_convert_season_id($service_name, $saison);
  $match_series = vbgame_sams_load_remote_match_series($service_name, $season_id);

  // Extract content!
  foreach ($match_series as $ms) {
    $name = check_plain((string) $ms->name);
    $id = check_plain((string) $ms->id);
    // Just return $name.
    $r[$id] = $name;
  }

  return $r;
}
/**
 * Validate Form Element Staffel.
 *
 * Callback for Teammanager
 * Nothing to do, we always get a valid one from server.
 */
function vbgame_sams_validate_form_element_staffel($service_name, $value) {
  // Element is always valid.
  return TRUE;
}
/**
 * Implements getStaffelOptions().
 *
 * Callback for sams_* backendes
 */
function vbgame_sams_getTeamOptions($service_name, $saison, $sid) {
  $xml = vbgame_sams_load_remote_team_list($service_name, $sid);

  // Extract content.
  for ($i = 0; $i != count($xml); ++$i) {
    $name = check_plain(trim((string) $xml[$i]->name));
    // Just return $name.
    $r[$name] = $name;
  }
  return $r;
}

/**
 * Convert Saison ID to SAMS Saison ID.
 *
 * This is a manual convert. And currently only for one backend,
 * due to lack of testing capabilities.
 */
function vbgame_sams_convert_season_id($service_name, $saison) {
  $dvv_transform = array(
    '2013' => '108',
    '2014' => '954762',
    '2015' => '1034164',
  );

  switch ($service_name) {
    case 'DVV':
      return $dvv_transform[$saison];

    default:
      return $saison;
  }
}

/**
 * Checks if update is necessary.
 *
 * Honor SAMS fetch policy!
 */
function vbgame_sams_check_should_update($service_name, $teaminfo) {
  $cache_date = $teaminfo->xmlfetchtime;
  $season_id = vbgame_sams_convert_season_id($service_name, $teaminfo->jahr);
  $match_series = vbgame_sams_load_remote_match_series($service_name, $season_id);
  $match_series = $match_series[$teaminfo->sid];

  if (empty($match_series)) {
    watchdog('vbgame_sams', t('matchSeriesId is not in fetched record.'));
    return FALSE;
  }

  $structure_updated = strtotime($cache_date) < strtotime((string) $match_series->structureUpdated);
  $results_updated = strtotime($cache_date) < strtotime((string) $match_series->resultsUpdated);

  $update_allowed = ($structure_updated OR $results_updated);
  if ($update_allowed) {
    watchdog('vbgame_sams', t('update allowed'));
  }

  return $update_allowed;
}
/**
 * Implements vlw_fetch_all().
 *
 * retrieve remote content and return in structured form.
 */
function vbgame_sams_fetch_all($service_name, $teaminfo) {
  drupal_set_message('SAMS FETCH ALL');
  // Fetch new content for team.
  $sid = $teaminfo->sid;

  $content['spielplan'] = vbgame_sams_load_remote_spielplan($service_name, $sid);

  switch ($teaminfo->rankingtype) {
    case 'twopoint':
      $content['ranking'] = vbgame_sams_load_remote_tabelle($service_name, $sid);
      break;

    case 'threepoint':
      $content['ranking'] = vbgame_sams_load_remote_threepoint_ranking($service_name, $sid);
      break;
  }

  return $content;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++ XML RETRIEVAL FUNCTIONS +++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 * XML retrieval function.
 *
 * Shows a list of all available Seasons.
 */
function vbgame_sams_load_remote_season_overview($service_name) {
  $encoding = 'utf-8';

  $api_key = vbgame_sams_getApiKey($service_name);
  $url = vbgame_sams_interface_url($service_name) . 'xml/seasons.xhtml?apiKey=' . $api_key;

  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  $xml = simplexml_load_string($content);

  return $xml->season;
}

/**
 * XML retrieval function.
 *
 * Returns information about a matchseries.
 */
function vbgame_sams_load_remote_match_series($service_name, $season_id = NULL) {
  $encoding = 'utf-8';

  $api_key = vbgame_sams_getApiKey($service_name);
  $url = vbgame_sams_interface_url($service_name) . 'xml/matchSeries.xhtml?apiKey=' . $api_key;
  if ($season_id !== NULL) {
    $url .= '&seasonId=' . $season_id;
  }

  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  $xml = simplexml_load_string($content);

  // Have matchSeriesId as Key.
  foreach ($xml->matchSeries as $ms) {
    $match_series_id = check_plain((string) $ms->id);
    $match_series[$match_series_id] = $ms;
  }

  return $match_series;
}

/**
 * XML retrieval function.
 *
 * Returns a Team Listing
 */
function vbgame_sams_load_remote_team_list($service_name, $match_series_id = NULL) {
  $encoding = 'utf-8';

  $api_key = vbgame_sams_getApiKey($service_name);
  $url = vbgame_sams_interface_url($service_name) . 'xml/teams.xhtml?apiKey=' . $api_key;
  if ($match_series_id !== NULL) {
    $url .= '&matchSeriesId=' . $match_series_id;
  }

  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  $xml = simplexml_load_string($content);

  return $xml->team;
}
/**
 * XML retrieval function.
 *
 * Returns Spielplan and Results
 */
function vbgame_sams_load_remote_matchSeries_schedule($service_name, $match_series_id = NULL, $team_id = NULL) {
  $encoding = 'utf-8';

  $api_key = vbgame_sams_getApiKey($service_name);
  $url = vbgame_sams_interface_url($service_name) . 'xml/matchSeries.xhtml?apiKey=' . $api_key;
  if ($match_series_id !== NULL) {
    $url .= '&matchSeriesId=' . $match_series_id;
  }
  if ($team_id !== NULL) {
    $url .= '&teamId=' . $team_id;
  }
  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  $xml = simplexml_load_string($content);

  return $xml->match;
}
/**
 * XML retrieval function.
 *
 * fetches Ranking.
 */
function vbgame_sams_load_remote_tabelle($service_name, $match_series_id) {
  $encoding = 'utf-8';

  $api_key = vbgame_sams_getApiKey($service_name);
  $url = vbgame_sams_interface_url($service_name) . 'xml/rankings.xhtml?apiKey=' . $api_key . '&matchSeriesId=' . $match_series_id;

  // Fetch XML.
  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  $xml = simplexml_load_string($content);

  // Insert new content.
  $i = 0;
  foreach ($xml->ranking as $ranking) {
    $r[$i]['platz'] = check_plain((string) $ranking->place);
    $r[$i]['team'] = check_plain((string) $ranking->team->name);
    $r[$i]['spiele'] = check_plain((string) $ranking->matchesPlayed);
    $points = explode(':', (string) $ranking->points);
    $saetze = explode(':', (string) $ranking->setPoints);
    $baelle = explode(':', (string) $ranking->ballPoints);
    $r[$i]['pluspunkte'] = check_plain($points[0]);
    $r[$i]['minuspunkte'] = check_plain($points[1]);
    $r[$i]['plussaetze'] = check_plain($saetze[0]);
    $r[$i]['minussaetze'] = check_plain($saetze[1]);
    $r[$i]['plusbaelle'] = check_plain($baelle[0]);
    $r[$i]['minusbaelle'] = check_plain($baelle[1]);
    $i++;
  }
  return $r;
}
/**
 * XML retrieval function.
 *
 * fetches 3-Point Rule Ranking.
 */
function vbgame_sams_load_remote_threepoint_ranking($service_name, $match_series_id) {
  $encoding = 'utf-8';

  $api_key = vbgame_sams_getApiKey($service_name);
  $url = vbgame_sams_interface_url($service_name) . 'xml/rankings.xhtml?apiKey=' . $api_key . '&matchSeriesId=' . $match_series_id;

  // Fetch XML.
  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  $xml = simplexml_load_string($content);

  // Insert new content.
  $i = 0;
  foreach ($xml->ranking as $ranking) {

    $r[$i]['team'] = check_plain((string) $ranking->team->name);
    $r[$i]['place'] = check_plain((string) $ranking->place);
    $r[$i]['matchesPlayed'] = check_plain((string) $ranking->matchesPlayed);

    $r[$i]['wins'] = check_plain($ranking->wins);
    $r[$i]['losses'] = check_plain($ranking->losses);
    $r[$i]['points'] = check_plain($ranking->points);

    $r[$i]['setPoints'] = check_plain($ranking->setPoints);
    $r[$i]['setWinScore'] = check_plain($ranking->setWinScore);
    $r[$i]['setLoseScore'] = check_plain($ranking->setLoseScore);
    $r[$i]['setPointDifference'] = check_plain($ranking->setPointDifference);
    $r[$i]['setQuotient'] = check_plain($ranking->setQuotient);

    $r[$i]['ballPoints'] = check_plain($ranking->ballPoints);
    $r[$i]['ballWinScore'] = check_plain($ranking->ballWinScore);
    $r[$i]['ballLoseScore'] = check_plain($ranking->ballLoseScore);
    $r[$i]['ballPointDifference'] = check_plain($ranking->ballPointDifference);
    $r[$i]['ballQuotient'] = check_plain($ranking->ballQuotient);

    foreach ($ranking->resultTypes->matchResult as $mr) {
      switch ($mr->result) {
        case '3:0':
          $r[$i]['count30'] = check_plain($mr->count);
          break;

        case '3:1':
          $r[$i]['count31'] = check_plain($mr->count);
          break;

        case '3:2':
          $r[$i]['count32'] = check_plain($mr->count);
          break;

        case '2:3':
          $r[$i]['count23'] = check_plain($mr->count);
          break;

        case '1:3':
          $r[$i]['count13'] = check_plain($mr->count);
          break;

        case '0:3':
          $r[$i]['count03'] = check_plain($mr->count);
          break;
      }
    }

    $i++;
  }
  return $r;
}
/**
 * Implements updateSpielplanLiga().
 *
 * helper function for updateXMLCache
 * fetches/updates Season Schedules and Results
 */
function vbgame_sams_load_remote_spielplan($service_name, $match_series_id, $remote_team_id = NULL) {
  $encoding = 'utf-8';

  $api_key = vbgame_sams_getApiKey($service_name);
  $url = vbgame_sams_interface_url($service_name) . 'xml/matches.xhtml?apiKey=' . $api_key . '&matchSeriesId=' . $match_series_id;

  if (!empty($remote_team_id)) {
    $url .= '&teamId=' . $remote_team_id;
  }

  // Fetch schedule XML.
  $request = drupal_http_request($url);
  $content = drupal_convert_to_utf8($request->data, $encoding);
  $xml = simplexml_load_string($content);

  // Create content.
  $i = 0;
  foreach ($xml->match as $match) {

    $r[$i]['nr'] = check_plain((string) $match->number);
    $r[$i]['datum'] = check_plain((string) $match->date);
    $r[$i]['beginn'] = check_plain((string) $match->time);

    foreach ($match->team as $team) {
      if ((int) $team->number == 1) {
        $heim = $team->name;
      }
      elseif ((int) $team->number == 2) {
        $gast = $team->name;
      }
    }

    $r[$i]['heim'] = check_plain((string) $heim);
    $r[$i]['gast'] = check_plain((string) $gast);
    unset($heim);
    unset($gast);

    if (!empty($match->results)) {
      $setpoints = check_plain((string) $match->results->setPoints);
      $saetze = explode(':', $setpoints);
      $r[$i]['sheim'] = check_plain($saetze[0]);
      $r[$i]['sgast'] = check_plain($saetze[1]);

      foreach ($match->results->sets->set as $set) {
        $result[(int) $set->number] = check_plain((string) $set->points);
      }
      $r[$i]['result'] = $setpoints . ' (' . implode(', ', $result) . ')';
      unset($result);
      unset($saetze);
      unset($setpoints);
    }
    else {
      $r[$i]['sheim'] = NULL;
      $r[$i]['sgast'] = NULL;
      $r[$i]['result'] = NULL;
    }
    $r[$i]['halle'] = check_plain((string) $match->location->name);
    $i++;
  }
  return $r;
}
