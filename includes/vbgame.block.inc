// +++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++ Block Functions ++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 * Implements hook_block_info().
 *
 * Define 4 Blocks:
 *    1. vbgamesumtab: display block with summary ranking
 *    2. vbgameprecog: display block with next game information
 *    3. vbgameteamtab: display block with ranking for one team only
 *    4. vbgameteamprecog: display block with next game info for one team only
 */
function vbgame_block_info() {
  $blocks['vbgame_kurztabelle'] = array(
    'info' => t('@moduledesc: ranking overview', array('@moduledesc' => 'Volleyball')),
    'cache' => 'BLOCK_CACHE_PER_ROLE',
  );
  $blocks['vbgame_vorschau'] = array(
    'info' => t('@moduledesc: preview', array('@moduledesc' => 'Volleyball')),
    'cache' => 'BLOCK_CACHE_PER_ROLE',
  );
  $blocks['vbteamtab'] = array(
    'info' => t('@moduledesc: team ranking', array('@moduledesc' => 'Volleyball')),
    'cache' => 'BLOCK_CACHE_PER_ROLE',
  );
  $blocks['vbteamprecog'] = array(
    'info' => t('@moduledesc: team preview', array('@moduledesc' => 'Volleyball')),
    'cache' => 'BLOCK_CACHE_PER_ROLE',
  );
  $blocks['vbgame_next_promoted_game'] = array(
    'info' => t('@moduledesc: next game (promoted team)', array('@moduledesc' => 'Volleyball')),
    'cache' => 'BLOCK_CACHE_PER_ROLE',
  );
  return $blocks;
}

/**
 * Implements hook_block_config().
 */
function vbgame_block_config($delta = '') {
  switch ($delta) {
    case 'vbgame_next_promoted_game':
      $form['vbgame_next_promoted_game_teamid'] = array(
        '#type' => 'select',
        '#title' => t('Team ID'),
        '#options' => vbgame_get_form_team_options(),
        '#default_value' => variable_get('vbgame_next_promoted_game_teamid', NULL),
        '#size' => 1,
        '#weight' => -4,
        '#required' => TRUE);
      return $form;

    case 'vbgame_block_nogames':
      $form['vbgame_block_nogames'] = array(
        '#type' => 'textfield',
        '#title' => t('No Games Text'),
        '#size' => 20,
        '#default_value' => variable_get('vbgame_block_nogames', t('No matchups scheduled in this matchseries for the upcoming week.')),
      );
      return $form;
  }
}

/**
 * Implements hook_block_save().
 */
function vbgame_block_save($delta, $edit) {
  switch ($delta) {
    case 'vbgame_next_promoted_game':
      variable_set('vbgame_next_promoted_game_teamid', check_plain($edit['vbgame_next_promoted_game_teamid']));
      break;
  }
}

/**
 * Implements hook_block_view().
 */
function vbgame_block_view($delta = '') {
  switch ($delta) {
    case 'vbgame_kurztabelle':
      $active_season = config_get('vbgame.settings', 'active_season');
      $block['subject'] = t('Rankings @saison', array('@saison' => $active_season));
      $block['content'] = theme('vbgame_summarypage_content', array(
        'type' => 'kurztabelle',
        'content' => vbgame_summary_content('kurztabelle', $active_season),
      ));
      return $block;

    case 'vbgame_vorschau':
      $active_season = config_get('vbgame.settings', 'active_season');
      $content = vbgame_summary_content('vorschau', $active_season);
      $block['subject'] = t('preview');
      if (empty($content)) {
        $block['content'] = variable_get('vbgame_block_nogames', t('No official matchups in the upcoming week.'));
      }
      else {
        $block['content'] = theme('vbgame_summarypage_content', array(
          'type' => 'vorschau',
          'content' => $content,
        ));
      }
      return $block;

    // Next game (promoted team).
    case 'vbgame_next_promoted_game':
      $teamid = variable_get('vbgame_next_promoted_game_teamid', NULL);
      $next_games = vbgame_select_next_n_matches($teamid, 1);

      $block['subject'] = t('Next TOP match');
      if (empty($next_games)) {
        $block['content'] = variable_get('vbgame_block_nogames', t('No official matchups in the upcoming week.'));
      }
      else {
        $block['content'] = theme('vbgame_topmatch_preview', $next_games);
      }
      return $block;
  }
}

