// +++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++ NODETYPE TeamPage Functions +++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**
 * Implements hook_form().
 *
 * Nodetype Teampage
 */
function vbgame_teampage_form($node, &$form_state) {

  $type = node_type_get_name($node);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#default_value' => !empty($node->title) ? $node->title : check_plain($type->title_label),
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['vbgame_teamid'] = array(
    '#type' => 'select',
    '#title' => t('Team ID'),
    '#options' => vbgame_get_form_team_options(),
    '#default_value' => !empty($node->vbgame_teamid) ? $node->vbgame_teamid : '',
    '#size' => 1,
    '#weight' => -4,
    '#required' => TRUE);

  if ($type->has_body) {
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }

  $form['vbgame_type'] = array(
    '#type' => 'select',
    '#title' => t('page type'),
    '#options' => vbgame_get_options('team_page_types'),
    '#default_value' => !empty($node->vbgame_type) ? $node->vbgame_type : '',
    '#size' => 1,
    '#required' => TRUE);

  return $form;
}
/**
 * Implements hook_insert().
 *
 * Nodetype Teampage
 */
function vbgame_teampage_insert($node) {
  $object = new stdClass();
  $object->nid = $node->nid;
  $object->type = $node->vbgame_type;
  $object->teamid = $node->vbgame_teamid;

  drupal_write_record('vbgame_teampage', $object);
}
/**
 * Implements hook_update().
 *
 * Nodetype Teampage
 */
function vbgame_teampage_update($node) {
  $object = new stdClass();
  $object->nid = $node->nid;
  $object->type = $node->vbgame_type;
  $object->teamid = $node->vbgame_teamid;

  drupal_write_record('vbgame_teampage', $object, 'nid');
}
/**
 * Implements hook_delete().
 *
 * Nodetype teampage
 */
function vbgame_teampage_delete($node) {
  db_query('DELETE FROM {vbgame_teampage} WHERE nid = %d', $node->nid);
}
/**
 * Implements hook_load().
 *
 * Nodetype Teampage
 */
function vbgame_teampage_load($node) {
  $additions = db_query('SELECT type as vbgame_type, teamid as vbgame_teamid FROM {vbgame_teampage} WHERE nid = :nid',
    array(':nid' => $node->nid));
  return $additions;
}
/**
 * Implements hook_view().
 *
 * Nodetype teampage
 */
function vbgame_teampage_view($node, $view_mode) {
  $mynode = node_prepare($node, FALSE);

  switch ($node->vbgame_type) {
    case 'threepoint_ranking_all':
      $mynode->content['body'] = array(
        '#value' => theme('vbgame_ranking_threepoint_all', array(
          'content' => vbgame_team_content($node->vbgame_type, $node->vbgame_teamid),
          'teaminfo' => vbgame_team_info($node->vbgame_teamid),
        )),
        '#weight' => 1,
      );
      break;

    default:
      $mynode->content['body'] = array(
        '#value' => theme('vbgame_team_content', array(
          'type' => $node->vbgame_type,
          'teaminfo' => vbgame_team_info($node->vbgame_teamid),
          'content' => vbgame_team_content($node->vbgame_type, $node->vbgame_teamid),
        )),
        '#weight' => 1,
      );
  }
  return $mynode;
}
